package ru.vkandyba.tm.exception.entity;

import ru.vkandyba.tm.exception.AbstractException;

public class UserNotFoundException extends AbstractException {

    public UserNotFoundException() {
        super("Error! User not found!");
    }
}
