package ru.vkandyba.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.vkandyba.tm.api.entity.IWBS;
import ru.vkandyba.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class Task extends AbstractBusinessEntity{

    private String name;

    private String description;

    private Status status = Status.NON_STARTED;

    private String projectId = null;

    private Date startDate;

    private Date finishDate;

    private Date createdDate = new Date();

    public Task(String name, String description) {
        this.name = name;
        this.description = description;
    }


    @Override
    public String toString() {
        return id + ": " + name;
    }

}
