package ru.vkandyba.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.command.AbstractCommand;
import ru.vkandyba.tm.enumerated.Role;

public class CommandsCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "commands";
    }

    @NotNull
    @Override
    public String arg() {
        return "-cmd";
    }

    @NotNull
    @Override
    public String description() {
        return "Display list of commands...";
    }

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        for (final AbstractCommand command : serviceLocator.getCommandService().getCommands()) {
            @Nullable final String commandName = command.name();
            if (commandName != null && !commandName.isEmpty())
                System.out.println(commandName + ": " + command.description());
        }
    }

}
