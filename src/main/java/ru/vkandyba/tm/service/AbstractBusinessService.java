package ru.vkandyba.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.api.repository.IBusinessRepository;
import ru.vkandyba.tm.exception.empty.EmptyIdException;
import ru.vkandyba.tm.exception.empty.EmptyIndexException;
import ru.vkandyba.tm.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractBusinessService<E extends AbstractBusinessEntity>
        extends AbstractService<E> implements IBusinessRepository<E> {

    @NotNull
    protected IBusinessRepository<E> repository;

    public AbstractBusinessService(@NotNull IBusinessRepository<E> repository) {
        super(repository);
        this.repository = repository;
    }

    @NotNull
    @Override
    public List findAll(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        return repository.findAll(userId);
    }

    @NotNull
    @Override
    public List findAll(@Nullable String userId, @Nullable Comparator comparator) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (comparator == null) throw new RuntimeException();
        return repository.findAll(userId, comparator);
    }

    @NotNull
    @Override
    public Boolean existsByIndex(@Nullable String userId, @Nullable Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        return repository.existsByIndex(userId, index);
    }

    @NotNull
    @Override
    public Boolean existsById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.existsById(userId, id);
    }

    @Nullable
    @Override
    public E findById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(userId, id);
    }

    @Nullable
    @Override
    public E findByIndex(@Nullable String userId, @Nullable Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        return repository.findByIndex(userId, index);
    }

    @NotNull
    @Override
    public E removeById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.removeById(userId, id);
    }

    @NotNull
    @Override
    public E removeByIndex(@Nullable String userId, @Nullable Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        return repository.removeByIndex(userId, index);
    }

    @NotNull
    @Override
    public E add(@Nullable String userId, @Nullable E entity) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (entity == null) throw new RuntimeException();
        return repository.add(userId, entity);
    }

    @NotNull
    @Override
    public void remove(@Nullable String userId, @Nullable E entity) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (entity == null) throw new RuntimeException();
        repository.remove(userId, entity);
    }

}
